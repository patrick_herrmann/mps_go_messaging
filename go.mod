module bitbucket.org/patrick_herrmann/mps_go_messaging

go 1.17

require (
	bitbucket.org/patrick_herrmann/mps_go_core v0.3.1 // indirect
	github.com/confluentinc/confluent-kafka-go v1.8.2 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
