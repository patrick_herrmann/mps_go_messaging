/*
This module provides functionalities to both publish and consume messages
from a queue.
*/
package messaging

import (
	"fmt"

	utils "bitbucket.org/patrick_herrmann/mps_go_core"
	kafka "github.com/confluentinc/confluent-kafka-go/kafka"
)

/*
Publisher should be used to push a new message to a broker that will be
later on consumed by a consumer (see GetCustomer).
*/
type Publisher struct {
	impl   *kafka.Producer
	status connectionStatus
	utils.AnyWithId
}

/*
GetPublisher returns a publisher to be used to post a message to a queue.

The new consumer setup is defined by the given config. As of now, it only
implements confluent, so the configuration should look like:
	confluent:
		ConfigMap:
			bootstrap.servers: "localhost"

The ConfigMap property stores the configurations for a Confluent client for
a Kafka queue; more details here:
https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md.
*/
func GetPublisher(config utils.Config) (*Publisher, error) {
	p := &Publisher{
		status: connectionStatus(connectionClose),
	}
	p.InitWithConfig(config)
	// Unmarshal the confluent configurations set
	var err error
	p.impl, err = kafka.NewProducer(unmarshalConfluentConfiguration(*p.Config))
	if err != nil {
		return nil, fmt.Errorf("Could not instantiate the publisher implementation: %v.", err)
	}
	p.status = connectionStatus(connectionOpen)
	return p, nil
}

/*
Post publishes a message to a queue.

The given message is eventually published into the given queue / topic. This
means that when this function returns, it does not necessarily mean that the
message has already been published. The action is performed asynchronously.
*/
func (p *Publisher) Post(msg *Message) error {
	if p.impl == nil {
		return fmt.Errorf("Cannot post a message to topic %s: there is no underlying publisher instantiated.", msg.Topic)
	}
	if p.status == connectionStatus(connectionClose) {
		return fmt.Errorf("Connection has been closed.")
	}
	if err := p.impl.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &(msg.Topic), Partition: kafka.PartitionAny},
		Value:          msg.Content,
	}, nil); err != nil {
		return fmt.Errorf("Cannot post a message to topic %s: %v.", msg.Topic, err)
	} else {
		return nil
	}
}

/*
Close the publisher, releasing all attached resources, including the
underlying current implementation.
*/
func (p *Publisher) Close() {
	p.Logger.Info("Close", "Closing publisher %d.", p.Id)
	p.status = connectionStatus(connectionClose)
	p.impl.Close()
}
