/*
This module provides functionalities to both publish and consume messages
from a queue.
*/
package messaging

import (
	utils "bitbucket.org/patrick_herrmann/mps_go_core"
	kafka "github.com/confluentinc/confluent-kafka-go/kafka"
)

/*
Message wraps information to be published or consumed from a queue.

Topic is the specific queue to use to publish or consume the message from.

Content is the bytes-encoded content for the message. It is convenient here
to use some serialisation strategy, for example JSON serialisation, to make
it easy to decode by the consumer.
*/
type Message struct {
	Topic   string
	Content []byte
}

type connectionStatus int8

const (
	connectionClose = int8(0)
	connectionOpen  = int8(1)
)

// unmarshalConfluentConfiguration extracts, From the given set of
// properties attached, the Confluent Kafka client setup (kafka.ConfigMap)
// and returns it.
//
// More about Confluent ConfigMap setup:
// https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
func unmarshalConfluentConfiguration(config utils.Config) *kafka.ConfigMap {
	configMap := &kafka.ConfigMap{}
	conf := config.GetWithFallback(map[string]interface{}{}, "confluent", "ConfigMap")
	if m, ok := conf.(map[interface{}]interface{}); ok {
		for key, val := range m {
			(*configMap)[key.(string)] = val
		}
	}
	return configMap
}
