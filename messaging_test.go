package messaging

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"

	utils "bitbucket.org/patrick_herrmann/mps_go_core"
)

type TestMessage struct {
	Key   string
	Value string
}

func TestMain(m *testing.M) {
	cf := utils.GetConfig()
	err := cf.Unmarshal([]byte(`
publishers:
  localPublisher:
    confluent:
      ConfigMap:
        bootstrap.servers: "localhost:9093"
consumers:
  localConsumer:
    confluent:
      ConfigMap:
        bootstrap.servers: "localhost:9093"
        group.id: "test"
        # Use latest offset by default to make sure to read the message that
        # was posted at the latest - preventing generating false positive if
        # previous tests failed after posting a message but before consuming
        # it.
        auto.offset.reset: "latest"`))
	if err != nil {
		panic(fmt.Sprintf("Cannot start the auto. test: %v.", err))
	}
	os.Exit(m.Run())
}

func TestPublishingAndConsumingAMessage(t *testing.T) {
	t.Log("Testing publishing a message to a messages broker and consuming it right after.")
	// Get a new publisher for a given configuration.
	publisher, err := GetPublisher(*utils.GetConfigForTag("publishers", "localPublisher"))
	if err != nil {
		t.Fatalf("TestPublishingAndConsumingAMessage(could not get a new publisher: %v)", err)
	}
	// Make sure allocated resources are cleared when leaving this test.
	defer publisher.Close()
	// Marshal some msgJson content to post it as a message to a queue.
	msgJson, _ := json.Marshal(&TestMessage{Key: "KEY", Value: "VALUE"})
	t.Log("TestPublishingAndConsumingAMessage(write message:", string(msgJson), " to the queue)")
	if err := publisher.Post(&Message{
		Topic:   "TEST",
		Content: msgJson,
	}); err != nil {
		t.Fatalf("TestPublishingAndConsumingAMessage(could not publish message: %v)", err)
	}

	// Get a new consumer for a given configuration.
	consumer, err := GetConsumer(*utils.GetConfigForTag("consumers", "localConsumer"))
	if err != nil {
		t.Fatalf("TestPublishingAndConsumingAMessage(could not get a new consumer: %v)", err)
	}
	// Make sure allocated resources are cleared when leaving this test.
	defer consumer.Close()

	if msg, err := consumer.Get("TEST", 10*time.Second); err != nil {
		t.Fatalf("TestPublishingAndConsumingAMessage(could not consume the message: %v)", err)
	} else {
		t.Log("TestPublishingAndConsumingAMessage(message read from the queue:", string(msg.Content), ")")
		var testMsg TestMessage
		if err := json.Unmarshal(msg.Content, &testMsg); err != nil {
			t.Fatalf("TestPublishingAndConsumingAMessage(could not unmarshal the consumed message %s: %v)", string(msg.Content), err)
		} else if testMsg.Key != "KEY" || testMsg.Value != "VALUE" {
			t.Fatalf("TestPublishingAndConsumingAMessage(The consumed message %s did not properly unmarshaled: either of its attribute does not have the expected value)", string(msg.Content))
		}
	}
}

func TestTimeoutTryingToConsumeAMessage(t *testing.T) {
	t.Log("Testing the custom timeout error when trying to consume a message from a messages broker.")
	// Get a new consumer for a given configuration.
	consumer, err := GetConsumer(*utils.GetConfigForTag("consumers", "localConsumer"))
	if err != nil {
		t.Fatalf("TestTimeoutTryingToConsumeAMessage(could not get a new consumer: %v)", err)
	}
	// Make sure allocated resources are cleared when leaving this test.
	defer consumer.Close()

	if _, err := consumer.Get("TEST", 10*time.Second); err != nil {
		if _, ok := err.(*TimeoutError); !ok {
			t.Fatalf("TestTimeoutTryingToConsumeAMessage(raised error (%s) is not of the right type: %T)", err, err)
		}
	} else {
		t.Fatalf("TestTimeoutTryingToConsumeAMessage(timeout error not raised)")
	}
}
