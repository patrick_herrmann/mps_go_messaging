/*
This module provides functionalities to both publish and consume messages
from a queue.
*/
package messaging

import (
	"fmt"
	"time"

	utils "bitbucket.org/patrick_herrmann/mps_go_core"
	kafka "github.com/confluentinc/confluent-kafka-go/kafka"
)

/*
Consumer consumes messages from a messages broker; use a publisher (see
GetPublisher) to posts messages that can be later on consumed by such a
Consumer.
*/
type Consumer struct {
	impl   *kafka.Consumer
	topics map[string]bool
	status connectionStatus
	utils.AnyWithId
}

type TimeoutError struct {
	time time.Duration
}

func (err *TimeoutError) Error() string {
	return fmt.Sprintf("Waited %s, but did not get a message.", err.time)
}

/*
GetConsumer returns a consumer to read messages from a queue from a messages
broker.

The new consumer setup is defined by the given config. As of now, it only
implements confluent, so the configuration should look like:
	confluent:
		ConfigMap:
			bootstrap.servers: "localhost"

The ConfigMap property stores the configurations for a Confluent client for
a Kafka queue; more details here:
https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md.
*/
func GetConsumer(config utils.Config) (*Consumer, error) {
	c := &Consumer{
		status: connectionStatus(connectionClose),
	}
	c.InitWithConfig(config)
	c.topics = make(map[string]bool)
	// Unmarshal the confluent configurations set
	var err error
	c.impl, err = kafka.NewConsumer(unmarshalConfluentConfiguration(*c.Config))
	if err != nil {
		return nil, fmt.Errorf("Could not instantiate the consumer implementation: %v.", err)
	}
	c.status = connectionStatus(connectionOpen)
	return c, nil
}

/*
Get fetches one single message from the messages broker for the given topic.

It returns the first retrieved message.

If the consumer cannot read any message for the given topic during the given
timeout, it fails with the custom TimeoutError.
*/
func (c *Consumer) Get(topic string, timeout time.Duration) (*Message, error) {
	if c.impl == nil {
		return nil, fmt.Errorf("Cannot get a topic for consumer %d: no implementation.", c.Id)
	}
	// Check if the consumer is already subscribed for the given topic. If
	// not, subscribe it.
	if _, ok := c.topics[topic]; !ok {
		c.Logger.Debug("Get", "Subscribe consumer %d to topic %s.", c.Id, topic)
		if topics, err := c.impl.Subscription(); err != nil {
			return nil, fmt.Errorf("Cannot get existing set of topics for consumer %d: %v.", c.Id, err)
		} else if err := c.impl.SubscribeTopics(append(topics, topic), nil); err != nil {
			return nil, fmt.Errorf("Cannot subscribe consumer %d to topic %s: %v.", c.Id, topic, err)
		}
		c.topics[topic] = true
	}
	if c.status == connectionStatus(connectionClose) {
		return nil, fmt.Errorf("Connection has been closed.")
	}
	// Read the next available message for the given topic.
	if msg, err := c.impl.ReadMessage(timeout); err != nil {
		if err.(kafka.Error).Code() == kafka.ErrTimedOut {
			return nil, &TimeoutError{time: timeout}
		} else {
			return nil, fmt.Errorf("Consumer %d cannot consume a message for topic %s: %v.", c.Id, topic, err)
		}
	} else {
		return &Message{Topic: *msg.TopicPartition.Topic, Content: msg.Value}, nil
	}
}

/*
Close the consumer, releasing all attached resources, including the
underlying current implementation.
*/
func (c *Consumer) Close() {
	c.Logger.Info("Close", "Closing consumer %d.", c.Id)
	c.status = connectionStatus(connectionClose)
	c.impl.Close()
}
